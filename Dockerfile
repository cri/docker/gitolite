# syntax = docker/dockerfile:experimental

FROM python:3.10 as base

ENV DEBIAN_FRONTEND=noninteractive \
    LC_ALL=C.UTF-8 \
    LANG=C.UTF-8 \
    PYTHONDONTWRITEBYTECODE=1 \
    PATH="/opt/venv/bin:$PATH"

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
            gettext-base moreutils \
            gitolite3 openssh-server dumb-init \
            iproute2 \
            sssd sssd-tools sudo libnss-sss \
            libldap2-dev libsasl2-dev \
            krb5-config krb5-multidev krb5-user libkrb5-dev \
            jq

RUN adduser --system --group --shell /bin/sh --home /var/lib/git git && \
    chown -R git:git ~git

FROM base as builder

RUN apt-get update && \
    apt-get install -y --no-install-recommends gcc python-dev

RUN --mount=type=cache,target=/root/.cache/pip \
    python -m venv /opt/venv && \
    pip3 install --upgrade pip && \
    pip3 install -U python-ldap environs

FROM base

COPY --from=builder /opt/venv/ /opt/venv/

# Volume used to store SSH host keys, generated on first run
VOLUME /etc/ssh/keys

# Volume used to store all Gitolite data (keys, config and repositories),
# initialized on first run
VOLUME /var/lib/git

ENV GIT_SHELL=/usr/share/gitolite3/gitolite-shell
COPY ./ldap_authorized_keys /usr/local/bin/ldap_authorized_keys
COPY ./ldap_get_groups /usr/local/bin/ldap_get_groups

COPY ./sshd_config /etc/ssh/sshd_config
COPY ./krb5.conf /etc/krb5.conf
COPY ./ldap.conf /etc/ldap/ldap.conf
COPY ./nsswitch.conf /etc/nsswitch.conf
COPY ./sssd.conf /etc/sssd/sssd.conf

COPY ./setup-container.sh /setup-container.sh
RUN /setup-container.sh && rm /setup-container.sh

COPY ./entrypoint.sh /entrypoint.sh
COPY ./stop.sh /stop.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD ["sshd"]
