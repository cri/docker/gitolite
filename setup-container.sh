#!/bin/sh

set -xe

# Hack to make openssl crap works
ln -s /etc/ssl/certs/ca-certificates.crt /usr/lib/ssl/cert.pem
ln -s /etc/ssl/certs/ca-certificates.crt /etc/ssl/cert.pem

chmod 555 /usr/local/bin/ldap_authorized_keys

echo 'ALL ALL=(git) NOPASSWD:ALL' >> /etc/sudoers
