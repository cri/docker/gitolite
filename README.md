# Gitolite

This image of gitolite add LDAP authentication to the base image `jgiannuzzi/gitolite`.

The container need to have a sidecar container with `sssd` that shares the
folder `/var/lib/sss/pipes`.
This image provide `sssd` for convenience. You can run the sssd sidecar with
the command `sssd -i`.

## Environement variables

* `LDAP_URI`: The ldap uri to contact the ldap, defaults to `ldaps://auth.int.cri.epita.net`.
* `LDAP_FILTER`: The ldap filter to search for user. It replace `{USERNAME_FILTER}` by `(uid=$USERNAME)`. By default the value of this varialbe is `(&(objectclass=ldapPublicKey)(objectclass=posixAccount){ USERNAME_FILTER })`.
* `LDAP_USER_DN`: The ldap DN in which the `LDAP_USER_FILTER` will perform, defaults `ou=users,dc=epita,dc=net`.
* `LDAP_GROUP_DN`: The ldap DN in which sssd search for groups, defaults `ou=groups,dc=epita,dc=net`.
* `LDAP_BASE`:  The base ldap DN, defaults to `dc=epita,dc=net`.
* `SSSD_CACHE_TIMEOUT`: Timeout in seconds for the cache of sssd, defaults to `600`
* `SSSD_ENUMERATE`: Whether or not sssd enumerate should be activated, defaults to `True`
* `CUSTOM_ENV`: A list of environment to write in the file `~/.environment`.
