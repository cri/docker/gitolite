#!/bin/sh

for i in $(seq 20); do
  if [ -z "$(ss -Hno state established '( dport = :ssh or sport = :ssh )')" ]; then
    break;
  else
    sleep 3;
    echo "This server is being stopped." > /etc/nologin;
  fi
done
