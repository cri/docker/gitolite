#!/bin/sh

set -xe

my_envsubst() {
    envsubst < "${1}" | sponge "${1}"
}

[ -z "$LDAP_URI" ] && \
    LDAP_URI="ldaps://ldap.pie.cri.epita.fr"

[ -z "$LDAP_BASE_DN" ] && \
    LDAP_BASE_DN="dc=cri,dc=epita,dc=fr"

[ -z "$LDAP_USER_FILTER" ] && \
    LDAP_USER_FILTER="(&(objectclass=ldapPublicKey)(objectclass=posixAccount))"

[ -z "$LDAP_USER_BASE_DN" ] && \
    LDAP_USER_BASE_DN="ou=users,$LDAP_BASE_DN"

[ -z "$LDAP_GROUP_BASE_DN" ] && \
    LDAP_GROUP_BASE_DN="ou=groups,$LDAP_BASE_DN"

[ -z "$LDAP_GROUP_FILTER" ] && \
    LDAP_GROUP_FILTER="(|(member=%d)(uniqueMember=%d)(memberUid=%u))"

[ -z "$SSSD_CACHE_TIMEOUT" ] && \
    SSSD_CACHE_TIMEOUT="600"

[ -z "$SSSD_ENUMERATE" ] && \
    SSSD_ENUMERATE="false"

[ -z "$KRB5_DEFAULT_REALM" ] && \
    KRB5_DEFAULT_REALM="CRI.EPITA.FR"

export LDAP_URI LDAP_BASE_DN LDAP_USER_FILTER LDAP_USER_BASE_DN \
    LDAP_GROUP_BASE_DN LDAP_GROUP_FILTER SSSD_CACHE_TIMEOUT SSSD_ENUMERATE \
    KRB5_DEFAULT_REALM

my_envsubst /etc/krb5.conf
my_envsubst /etc/sssd/sssd.conf
chmod 600 /etc/sssd/sssd.conf
my_envsubst /etc/ldap/ldap.conf

env | grep -v "HOME=" > /etc/environment
chmod 700 ~git

if [ -n "$LDAP_BIND_DN" ]; then
	echo "ldap_default_bind_dn = ${LDAP_BIND_DN}" >> /etc/sssd.conf
	if [ -n "$LDAP_BIND_PASSWORD_FILE" ]; then
		sss_obfuscate -s -d LDAP < "$LDAP_BIND_PASSWORD_FILE"
	else
		echo "ldap_default_authtok = $LDAP_BIND_PASSWORD" >> /etc/sssd.conf
	fi
fi

# if command is sshd, set it up correctly
if [ "${1}" = 'sshd' ]; then
  shift 1
  set -- dumb-init /usr/sbin/sshd -D -e "$@"

  # Setup SSH HostKeys if needed
  for algorithm in rsa ecdsa ed25519; do
    keyfile=/etc/ssh/keys/ssh_host_${algorithm}_key
    [ -f $keyfile ] || ssh-keygen -q -N '' -f $keyfile -t $algorithm
  done

  mkdir -p /run/sshd

  chown git:git ~git ~git/*
  chown -R git:git ~git/.gitolite ~git/.gitolite.rc ~git/repositories/gitolite-admin.git || true
  if [ "${DO_CHOWN:-0}" = "1" ]; then
    chown -R git:git ~git
  fi

  # Setup gitolite admin
  if [ "${DO_INITIAL_SETUP:-0}" = "1" ]; then
    if [ ! -f ~git/.ssh/authorized_keys ]; then
        if [ -n "$SSH_KEY" ]; then
            [ -n "$SSH_KEY_NAME" ] || SSH_KEY_NAME=admin
            echo "$SSH_KEY" > "/tmp/$SSH_KEY_NAME.pub"
            su - git -c "gitolite setup -pk \"/tmp/$SSH_KEY_NAME.pub\""
            rm "/tmp/$SSH_KEY_NAME.pub"
        else
            echo "You need to specify SSH_KEY on first run to setup gitolite"
            echo "You can also use SSH_KEY_NAME to specify the key name (optional)"
            echo 'Example: docker run -e SSH_KEY="$(cat ~/.ssh/id_rsa.pub)" -e SSH_KEY_NAME="$(whoami)" jgiannuzzi/gitolite'
            exit 1
        fi
    fi
  fi

  if [ "${DO_SETUP:-0}" = "1" ]; then
    su - git -c "gitolite setup"
  fi
fi

if [ "${1}" = "sssd" ]; then
  shift 1
  set -- sssd -i -d 0x01f0 "$@"
fi

exec "$@"
