#!/opt/venv/bin/python

import argparse
import ldap
import ldap.filter
from os import environ
from environs import Env

Env().read_env("/etc/environment")

def get_secret(name, default=None):
    alt_name = f"{name}_FILE"
    if name in environ:
        return environ.get(name)
    if alt_name in environ:
        with open(environ.get(alt_name), "r") as f:
            return f.read().strip()
    return default


LDAP_URI = environ["LDAP_URI"]
LDAP_BIND_DN = environ.get("LDAP_BIND_DN")
LDAP_BIND_PASSWORD = get_secret("LDAP_BIND_PASSWORD")

LDAP_USER_BASE_DN = environ["LDAP_USER_BASE_DN"]
LDAP_USER_FILTER = environ["LDAP_USER_FILTER"]
LDAP_USER_SCOPE = environ.get("LDAP_USER_SCOPE", "subtree")
LDAP_USER_USERNAME_ATTR = environ.get("LDAP_USER_USERNAME_ATTR", "uid")

LDAP_GROUP_BASE_DN = environ["LDAP_GROUP_BASE_DN"]
LDAP_GROUP_FILTER = environ["LDAP_GROUP_FILTER"]
LDAP_GROUP_SCOPE = environ.get("LDAP_GROUP_SCOPE", "subtree")
LDAP_GROUP_NAME_ATTR = environ.get("LDAP_GROUP_NAME_ATTR", "cn")

SCOPE_MAP = {
    "base": ldap.SCOPE_BASE,
    "onelevel": ldap.SCOPE_ONELEVEL,
    "subtree": ldap.SCOPE_SUBTREE,
}

def get_user_dn(con, username):
    user_filter = LDAP_USER_FILTER
    if username is not None:
        escaped_username = ldap.filter.escape_filter_chars(username)
        username_filter = f"({LDAP_USER_USERNAME_ATTR}={username})"
        user_filter = f"(&{username_filter}{user_filter})"

    results = con.search_s(
        LDAP_USER_BASE_DN,
        SCOPE_MAP[LDAP_USER_SCOPE],
        user_filter,
        []
    )

    if not results:
        raise RuntimeError("No users found!")

    if len(results) > 1:
        raise RuntimeError("Multiple users found!")

    return results[0][0]

def get_ldap_groups(con, username):
    group_filter = LDAP_GROUP_FILTER.replace(
        "%u",
        ldap.filter.escape_filter_chars(username),
    )

    if "%d" in group_filter:
        user_dn = get_user_dn(con, username)
        group_filter = LDAP_GROUP_FILTER.replace("%d", user_dn)

    results = con.search_s(
        LDAP_GROUP_BASE_DN,
        SCOPE_MAP[LDAP_GROUP_SCOPE],
        group_filter,
        [LDAP_GROUP_NAME_ATTR]
    )

    for dn, attrs in results:
        yield from [
            s.decode("utf-8")
            for s in attrs.get(LDAP_GROUP_NAME_ATTR, [])
        ]

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "user",
        help="retrieve ldap groups of the provided user",
    )
    args = parser.parse_args()
    username = args.user

    ldap.set_option(ldap.OPT_PROTOCOL_VERSION, 3)
    ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_DEMAND)
    con = ldap.initialize(LDAP_URI)

    if LDAP_BIND_DN:
        con.bind_s(
            LDAP_BIND_DN,
            LDAP_BIND_PASSWORD
        )

    groups = get_ldap_groups(con, username)
    print(*groups)
